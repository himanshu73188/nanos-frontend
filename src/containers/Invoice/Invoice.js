import React, { useEffect } from 'react';
import { connect } from 'react-redux';
import { useParams } from 'react-router-dom';

import axios from '../../axios';
import withErrorHandler from '../../hoc/withErrorHandler/withErrorHandler';
import * as actions from '../../store/actions/index';
import Spinner from '../../components/UI/Spinner/Spinner';
import InvoiceTemplate from '../../components/Invoice/InvoiceTemplate';

const Invoice = props => {

  const { onFetchInvoice } = props;
  const { campaignId }     = useParams();

  useEffect(() => {
    onFetchInvoice(campaignId);
  }, [onFetchInvoice, campaignId]);

  let invoice = <Spinner />;

  if (!props.loading) {
    if(props.invoice)
    invoice   = <InvoiceTemplate invoice={props.invoice} />;
    else
    invoice   = '';
  }

  return <React.Fragment>
    {invoice}
  </React.Fragment>;

};

const mapStateToProps = state => {
  return {
    invoice: state.invoice.invoice,
    loading: state.invoice.loading,
  };
};

const mapDispatchToProps = dispatch => {
  return {
    onFetchInvoice: (campaignId) =>
      dispatch(actions.fetchInvoice(campaignId))
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(withErrorHandler(Invoice, axios));