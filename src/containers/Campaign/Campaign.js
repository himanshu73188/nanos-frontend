import React, { useEffect } from 'react';
import { connect } from 'react-redux';

import axios from '../../axios';
import withErrorHandler from '../../hoc/withErrorHandler/withErrorHandler';
import * as actions from '../../store/actions/index';
import Spinner from '../../components/UI/Spinner/Spinner';
import CampaignsTable from '../../components/Campaign/CampaignsTable';

const Campaign = props => {

  const { onFetchCampaigns } = props;

  useEffect(() => {
    onFetchCampaigns();
  }, [onFetchCampaigns]);

  let campaigns = <Spinner />;

  if (!props.loading) {
    campaigns   = <CampaignsTable campaigns={props.campaigns} />;
  }

  return <React.Fragment>
    {campaigns}
  </React.Fragment>;

};

const mapStateToProps = state => {
  return {
    campaigns: state.campaign.campaigns,
    loading: state.campaign.loading,
  };
};

const mapDispatchToProps = dispatch => {
  return {
    onFetchCampaigns: () =>
      dispatch(actions.fetchCampaigns())
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(withErrorHandler(Campaign, axios));