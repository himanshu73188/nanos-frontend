import * as actionTypes from '../actions/actionTypes';
import { updateObject } from '../../shared/utility';

const initialState = {
    campaigns: [],
    loading: true,
};

const fetchCampaignsStart = ( state, action ) => {
    return updateObject( state, { loading: true } );
};

const fetchCampaignsSuccess = (state, action) => {
    return updateObject( state, {
        campaigns: action.campaigns,
        loading: false
    } );
};

const fetchCampaignsFail = (state, action) => {
    return updateObject( state, { loading: false } );
};

const reducer = ( state = initialState, action ) => {
    switch ( action.type ) { 
        case actionTypes.FETCH_CAMPAIGNS_START: return fetchCampaignsStart(state, action);
        case actionTypes.FETCH_CAMPAIGNS_SUCCESS: return fetchCampaignsSuccess(state, action);
        case actionTypes.FETCH_CAMPAIGNS_FAIL: return fetchCampaignsFail(state, action);
        default:
            return state;
    }
};

export default reducer;