import * as actionTypes from '../actions/actionTypes';
import { updateObject } from '../../shared/utility';

const initialState = {
    invoice: null,
    loading: true,
};

const fetchInvoiceStart = ( state, action ) => {
    return updateObject( state, { loading: true } );
};

const fetchInvoiceSuccess = (state, action) => {
    return updateObject( state, {
        invoice: action.invoice,
        loading: false
    } );
};

const fetchInvoiceFail = (state, action) => {
    return updateObject( state, { loading: false } );
};

const reducer = ( state = initialState, action ) => {
    switch ( action.type ) { 
        case actionTypes.FETCH_INVOICE_START: return fetchInvoiceStart(state, action);
        case actionTypes.FETCH_INVOICE_SUCCESS: return fetchInvoiceSuccess(state, action);
        case actionTypes.FETCH_INVOICE_FAIL: return fetchInvoiceFail(state, action);
        default:
            return state;
    }
};

export default reducer;