import * as actionTypes from './actionTypes';

export const fetchCampaignsStart = () => {
  return {
    type: actionTypes.FETCH_CAMPAIGNS_START
  };
};

export const fetchCampaignsSuccess = (campaigns) => {
  return {
    type: actionTypes.FETCH_CAMPAIGNS_SUCCESS,
    campaigns: campaigns.data
  };
};

export const fetchCampaignsFail = (error) => {
  return {
    type: actionTypes.FETCH_CAMPAIGNS_FAIL,
    error: error
  };
};


export const fetchCampaigns = () => {
  return {
    type: actionTypes.FETCH_CAMPAIGNS,
  };
};
