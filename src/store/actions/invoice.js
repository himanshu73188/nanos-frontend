import * as actionTypes from './actionTypes';

export const fetchInvoiceStart = () => {
  return {
    type: actionTypes.FETCH_INVOICE_START
  };
};

export const fetchInvoiceSuccess = (invoice) => {
  return {
    type: actionTypes.FETCH_INVOICE_SUCCESS,
    invoice: invoice.invoiceInfo
  };
};

export const fetchInvoiceFail = (error) => {
  return {
    type: actionTypes.FETCH_INVOICE_FAIL,
    error: error
  };
};


export const fetchInvoice = (campaignId) => {
  return {
    type: actionTypes.FETCH_INVOICE,
    campaignId: campaignId
  };
};
