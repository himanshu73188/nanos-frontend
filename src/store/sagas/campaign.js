import { put } from "redux-saga/effects";

import axios from "../../axios";
import * as actions from "../actions";

export function* fetchCampaignsSaga(action) {
  yield put(actions.fetchCampaignsStart());
  try {
    const response = yield axios.get('/v1/campaigns');
    const fetchedCampaigns = response.data;
    if(fetchedCampaigns.status === 'success') {
      yield put(actions.fetchCampaignsSuccess(fetchedCampaigns));
    } else {
      yield put(actions.fetchCampaignsFail('Something is wrong with the API call. Please try again. If you continue to have issue please notify the tech team.'));
    }
  } catch (error) {
    yield put(actions.fetchCampaignsFail(error));
  }
}
