import { takeEvery } from "redux-saga/effects";

import * as actionTypes from "../actions/actionTypes";
import { fetchCampaignsSaga } from "./campaign";
import { fetchInvoiceSaga } from "./invoice";

export function* watchCampaign() {
  yield takeEvery(actionTypes.FETCH_CAMPAIGNS, fetchCampaignsSaga);
}

export function* watchInvoice() {
  yield takeEvery(actionTypes.FETCH_INVOICE, fetchInvoiceSaga);
}
