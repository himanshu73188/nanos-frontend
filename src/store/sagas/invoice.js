import { put } from "redux-saga/effects";

import axios from "../../axios";
import * as actions from "../actions";

export function* fetchInvoiceSaga(action) {
  yield put(actions.fetchInvoiceStart());
  try {
    const response = yield axios.get('/v1/invoice/' + action.campaignId);
    const fetchedInvoice = response.data;
    console.log(fetchedInvoice);
    if(fetchedInvoice.status === 'success') {
      yield put(actions.fetchInvoiceSuccess(fetchedInvoice));
    } else {
      yield put(actions.fetchInvoiceFail('Something is wrong with the API call. Please try again. If you continue to have issue please notify the tech team.'));
    }
  } catch (error) {
    yield put(actions.fetchInvoiceFail(error));
  }
}
