import React, { Suspense } from 'react';
import { Route, Switch, withRouter, Redirect } from 'react-router-dom';

import Campaign from './containers/Campaign/Campaign';
import Invoice from './containers/Invoice/Invoice';

const app = props => {

  let routes = (
    <Switch>
      <Route path="/invoice/campaign/:campaignId" component={Invoice} />
      <Route path="/" exact component={Campaign} />
      <Redirect to="/" />
    </Switch>
  );

  return (
    <React.Fragment>
      <Suspense fallback={<p>Loading...</p>}>{routes}</Suspense>
    </React.Fragment>
  );
}

export default withRouter(app);
