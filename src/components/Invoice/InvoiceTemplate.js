import React from 'react';
import Button from '@material-ui/core/Button';
import { useHistory } from "react-router-dom";

// import classes from './InvoiceTemplate.module.css';

const InvoiceTemplate = ( props ) => {

    const history = useHistory();
    const invoice = props.invoice;

    return (
        <React.Fragment>
            <div className="frame invoice-template" >
            
                <div id="invoiceholder">
                  <div id="invoice" className="effect2">
    
                    <div id="invoice-top">
                      <div className="logo"><img src="https://nanos.ai/wp-content/uploads/2020/09/nanos_logo_WEaB.png" alt="Logo" /></div>
                      <div className="title">
                        <h1>INVOICE</h1>
                        <p>
                          Nanocorp AG<br/>
                          Stampfenbachstrasse 63<br/>
                          8006 Zurich, Switzerland<br/>
                          <a href="mailto:contact@nanos.ai">contact@nanos.ai</a><br/>
                           VAT: <span id="invoice_date">CHE-123456-TEST</span>
                        </p>
                      </div>
                    </div>
    
                    <div id="invoice-mid">   
                      <div id="message">
                        <h2>Hello { invoice.clientName },</h2>
                        <p>An invoice is created for campaign <span id="supplier_name">'{ invoice.campaignName }'</span> which is 100% matched with Nanos team and is waiting for your approval.</p>
                      </div>
                      <div className="clearfix">
                          <div className="col-left">
                              <div className="clientlogo"><img src="https://cdn3.iconfinder.com/data/icons/daily-sales/512/Sale-card-address-512.png" alt="Sup" /></div>
                              <div className="clientinfo">
                                  <h2 id="supplier">{ invoice.clientName }</h2>
                                  <p>
                                    <span id="address">Client Address</span><br/>
                                    {invoice.clientVat ? 
                                      <React.Fragment>
                                        <span id="city">VAT: { invoice.clientVat }</span><br/>
                                      </React.Fragment>
                                       : null
                                    }
                                  </p>
                              </div>
                          </div>
                          <div className="col-right">
                              <table className="table">
                                  <tbody>
                                      <tr>
                                          <td><span>Invoice Total</span><label id="invoice_total">{ invoice.invoiceAmount }</label></td>
                                          <td><span>Currency</span><label id="currency">{ invoice.invoiceCurrency }</label></td>
                                      </tr>
                                      { invoice.vatAmount > 0 ? <tr>
                                          <td><span>Net Amount</span><label id="payment_term">{ invoice.netAmount }</label></td>
                                          <td><span>VAT Amount</span><label id="invoice_type">{ invoice.vatAmount }</label></td>
                                      </tr> : null }
                                  </tbody>
                              </table>
                          </div>
                      </div>       
                    </div>
    
                    <div id="invoice-bot">
                      
                      <div id="table">
                        <table className="table-main">
                        { invoice.vatAmount > 0 ? 
                          <React.Fragment>
                          <thead>    
                            <tr className="tabletitle">
                              <th>Description</th>
                              <th>Duration</th>
                              <th>Taxable Amount</th>
                              <th>Tax Code</th>
                              <th>%</th>
                              <th>Tax Amount</th>
                              <th>Total</th>
                            </tr>
                          </thead>
                          <tbody>
                            <tr className="list-item">
                              <td data-label="Description" className="tableitem">{ invoice.campaignName }</td>
                              <td data-label="Duration" className="tableitem">30 Days</td>
                              <td data-label="Taxable Amount" className="tableitem">{ invoice.netAmount }</td>
                              <td data-label="Tax Code" className="tableitem">VAT</td>
                              <td data-label="%" className="tableitem">7.7</td>
                              <td data-label="Tax Amount" className="tableitem">{ invoice.vatAmount }</td>
                              <td data-label="Total" className="tableitem"><b>{ invoice.invoiceAmount }</b></td>
                            </tr>
                          </tbody>
                          </React.Fragment> : 
                          <React.Fragment>
                          <thead>    
                            <tr className="tabletitle">
                              <th>Description</th>
                              <th>Duration</th>
                              <th>Total</th>
                            </tr>
                          </thead>
                          <tbody>
                            <tr className="list-item">
                              <td data-label="Description" className="tableitem">{ invoice.campaignName }</td>
                              <td data-label="Duration" className="tableitem">30 Days</td>
                              <td data-label="Total" className="tableitem"><b>{ invoice.invoiceAmount }</b></td>
                            </tr>
                          </tbody>
                          </React.Fragment>
                        }
                        </table>
                      </div>
                      
                    </div>

                    <footer>
                    </footer>

                  </div>
                        
                  <Button variant="contained" color="primary" onClick={() => { history.push('/') }}>
                    Campaigns
                  </Button>

                </div>

            </div>
        </React.Fragment>
    );
};

export default InvoiceTemplate;