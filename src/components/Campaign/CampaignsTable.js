import React, { forwardRef } from 'react';
import MaterialTable from "material-table"; 
import { Search, Delete, FirstPage, LastPage, ChevronRight, ChevronLeft, Receipt, ArrowDownward } from "@material-ui/icons";
import { useHistory } from "react-router-dom";

import classes from './CampaignsTable.module.css';

const CampaignsTable = ( props ) => {

    const history = useHistory();

    const tableIcons = {
        Clear: forwardRef((props, ref) => <Delete {...props} ref={ref} />),
        Search: forwardRef((props, ref) => <Search {...props} ref={ref} />),
        ResetSearch: forwardRef((props, ref) => <Delete {...props} ref={ref} />),
        FirstPage: forwardRef((props, ref) => <FirstPage {...props} ref={ref} />),
        LastPage: forwardRef((props, ref) => <LastPage {...props} ref={ref} />),
        NextPage: forwardRef((props, ref) => <ChevronRight {...props} ref={ref} />),
        PreviousPage: forwardRef((props, ref) => <ChevronLeft {...props} ref={ref} />),
        SortArrow: forwardRef((props, ref) => <ArrowDownward {...props} ref={ref} />),
    };

    return (
        <React.Fragment>
            <div className="frame campaign-table" >
                <div className={classes.CampaignTable}>
                    <MaterialTable 
                        columns={[
                        { 
                            title: "Name", 
                            field: "name",
                        },
                        { 
                            title: "Customer", 
                            field: "customer_name",
                        },
                        ]}
                        data={props.campaigns}
                        title="Campaigns" 
                        icons={tableIcons}
                        actions={[
                            {
                                icon: () => <Receipt />,
                                tooltip: "Generate Invoice",
                                onClick: (event, rowData) => {
                                    history.push('/invoice/campaign/' + rowData.id);
                                }
                            }
                        ]}
                        options={{
                            headerStyle: {
                                backgroundColor: "#f5f5f5",
                            },
                            tableLayout: 'fixed'
                        }}
                    />
                </div>
            </div>
        </React.Fragment>
    );
};

export default CampaignsTable;