# Getting Started with Create React App

This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

## Requirements.

---

* ReactJs 17.0.2
* node v12.13.0
* npm 6.12.0
* Nanos API

## Dependencies.

---

* [@material-ui/core](https://www.npmjs.com/package/@material-ui/core) - Material is a design system created by Google to help teams build high-quality digital experiences for Android, iOS, Flutter, and the web.
* [@material-ui/icons](https://www.npmjs.com/package/@material-ui/icons) - This package provides the Google Material icons packaged as a set of React components.
* [@testing-library/jest-dom](https://www.npmjs.com/package/@testing-library/jest-dom) - The @testing-library/jest-dom library provides a set of custom jest matchers that you can use to extend jest. These will make your tests more declarative, clear to read and to maintain.
* [@testing-library/react](https://www.npmjs.com/package/@testing-library/react) - Simple and complete React DOM testing utilities that encourage good testing practices.
* [@testing-library/user-event](https://www.npmjs.com/package/@testing-library/user-event) - user-event tries to simulate the real events that would happen in the browser as the user interacts with it.
* [axios](https://www.npmjs.com/package/axios) - Promise based HTTP client for the browser and node.js
* [material-table](https://www.npmjs.com/package/material-table) - A simple and powerful Datatable for React based on Material-UI Table with some additional features.
* [node-sass](https://www.npmjs.com/package/node-sass) - It allows you to natively compile .scss files to css at incredible speed and automatically via a connect middleware.
* [react](https://www.npmjs.com/package/react) - React is a JavaScript library for creating user interfaces.
* [react-dom](https://www.npmjs.com/package/react-dom) - This package serves as the entry point to the DOM and server renderers for React. It is intended to be paired with the generic React package, which is shipped as react to npm.
* [react-redux](https://www.npmjs.com/package/react-redux) - Official React bindings for Redux.
* [react-router-dom](https://www.npmjs.com/package/react-router-dom) - DOM bindings for React Router.
* [react-scripts](https://www.npmjs.com/package/react-scripts) - This package includes scripts and configuration used by Create React App.
* [redux](https://www.npmjs.com/package/redux) - Redux is a predictable state container for JavaScript apps. 
* [redux-saga](https://www.npmjs.com/package/redux-saga) - An intuitive Redux side effect manager.
* [redux-thunk](https://www.npmjs.com/package/redux-thunk) - Thunk middleware for Redux.
* [web-vitals](https://www.npmjs.com/package/web-vitals) - The web-vitals library is a tiny (~1K), modular library for measuring all the Web Vitals metrics on real users, in a way that accurately matches how they're measured by Chrome and reported to other Google tool.

### How do I get set up? ###

---

Run commands in project terminal.

1. clone this repo via `git clone https://himanshu73188@bitbucket.org/himanshu73188/nanos-frontend.git`.

2. Run `cd nanos-frontend`.

3. Run `npm install`.

4. Run `npm start`.

5. Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

## Available Scripts

In the project directory, you can run:

### `npm start`

Runs the app in the development mode.\
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.\
You will also see any lint errors in the console.

### `npm test`

Launches the test runner in the interactive watch mode.\
See the section about [running tests](https://facebook.github.io/create-react-app/docs/running-tests) for more information.

### `npm run build`

Builds the app for production to the `build` folder.\
It correctly bundles React in production mode and optimizes the build for the best performance.

The build is minified and the filenames include the hashes.\
Your app is ready to be deployed!

See the section about [deployment](https://facebook.github.io/create-react-app/docs/deployment) for more information.

### `npm run eject`

**Note: this is a one-way operation. Once you `eject`, you can’t go back!**

If you aren’t satisfied with the build tool and configuration choices, you can `eject` at any time. This command will remove the single build dependency from your project.

Instead, it will copy all the configuration files and the transitive dependencies (webpack, Babel, ESLint, etc) right into your project so you have full control over them. All of the commands except `eject` will still work, but they will point to the copied scripts so you can tweak them. At this point you’re on your own.

You don’t have to ever use `eject`. The curated feature set is suitable for small and middle deployments, and you shouldn’t feel obligated to use this feature. However we understand that this tool wouldn’t be useful if you couldn’t customize it when you are ready for it.

## Learn More

You can learn more in the [Create React App documentation](https://facebook.github.io/create-react-app/docs/getting-started).

To learn React, check out the [React documentation](https://reactjs.org/).

### Code Splitting

This section has moved here: [https://facebook.github.io/create-react-app/docs/code-splitting](https://facebook.github.io/create-react-app/docs/code-splitting)

### Analyzing the Bundle Size

This section has moved here: [https://facebook.github.io/create-react-app/docs/analyzing-the-bundle-size](https://facebook.github.io/create-react-app/docs/analyzing-the-bundle-size)

### Making a Progressive Web App

This section has moved here: [https://facebook.github.io/create-react-app/docs/making-a-progressive-web-app](https://facebook.github.io/create-react-app/docs/making-a-progressive-web-app)

### Advanced Configuration

This section has moved here: [https://facebook.github.io/create-react-app/docs/advanced-configuration](https://facebook.github.io/create-react-app/docs/advanced-configuration)

### Deployment

This section has moved here: [https://facebook.github.io/create-react-app/docs/deployment](https://facebook.github.io/create-react-app/docs/deployment)

### `npm run build` fails to minify

This section has moved here: [https://facebook.github.io/create-react-app/docs/troubleshooting#npm-run-build-fails-to-minify](https://facebook.github.io/create-react-app/docs/troubleshooting#npm-run-build-fails-to-minify)

### Who do I talk to? ###

* Repo admin - himanshu73188@gmail.com
